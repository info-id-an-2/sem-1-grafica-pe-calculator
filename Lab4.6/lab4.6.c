#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include<math.h>
#include<stdio.h>


static int window;
static int menu_id;
static int submenu_id;
static int value = 0;
void menu(int num) {
	if (num == 0) {
		glutDestroyWindow(window);
		exit(0);
	}
	else {
		value = num;
	}
	glutPostRedisplay();
}
void createMenu(void) {
	submenu_id = glutCreateMenu(menu);
	glutAddMenuEntry("Sphere", 2);
	glutAddMenuEntry("Cone", 3);
	glutAddMenuEntry("Torus", 4);
	glutAddMenuEntry("Teapot", 5);
	glutAddMenuEntry("Cube", 6);
	glutAddMenuEntry("Dodecahedron", 7);
	glutAddMenuEntry("Octahedron", 8);
	glutAddMenuEntry("Tetrahedron", 9);
	glutAddMenuEntry("Icosahedrons", 10);
	menu_id = glutCreateMenu(menu);
	glutAddSubMenu("Draw", submenu_id);
	glutAddMenuEntry("Clear", 1);
	glutAddMenuEntry("Exit", 0);     glutAttachMenu(GLUT_RIGHT_BUTTON);
}
int GAngle = 0;
void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	if (value == 1) {
		return; //glutPostRedisplay();
	}
	else if (value == 2) {
		static float alpha = 20;
		glPushMatrix();
		glColor3d(1.0, 0.0, 0.0);
		glRotated(alpha, -1.0, 0.0, 0.0);
		glutWireSphere(0.5, 50, 50);
		glPopMatrix();
		alpha = alpha + 0.5;

	}
	else if (value == 3) {
		static float alpha = 20;
		glPushMatrix();
		glColor3d(0.0, 1.0, 0.0);
		glRotated(alpha, -1.0, 0.0, 0.0);
		glutWireCone(0.5, 1.0, 50, 50);
		glPopMatrix();
		alpha = alpha + 0.5;
	}
	else if (value == 4) {
		static float alpha = 20;

		glPushMatrix();
		glColor3d(0.0, 0.0, 1.0);
		glRotatef(alpha, 1.9, 0.6, 0);
		glutWireTorus(0.3, 0.6, 100, 100);
		glPopMatrix();
		alpha = alpha + 0.5;

	}
	else if (value == 5) {
		static float alpha = 20;

		glPushMatrix();
		glColor3d(1.0, 0.0, 1.0);
		glRotatef(alpha, 1.9, 0.6, 0);
		glutWireTeapot(0.5);
		glPopMatrix();
		alpha = alpha + 0.5;

	}
	else if (value == 6) {

		glColor3f(0.5, 0, 1);
		glLoadIdentity();
		glTranslated(-0.5, -0.5, 0.0);
		glRotated(GAngle, 1, 1, 0);
		glColor3f(1, 0, 0);
		glutWireCube(0.5);
		GAngle = GAngle + 1;
		glPopMatrix();


	}
	else if (value == 7) {
		static float alpha = 20;

		glPushMatrix();
		glColor3d(0.5, 0.0, 1.0);
		glRotatef(alpha, 1.9, 0.6, 0);
		glutSolidDodecahedron();
		glPopMatrix();
		alpha = alpha + 0.5;
	}
	else if (value == 8) {
		static float alpha = 20;

		glPushMatrix();
		glColor3d(0.0, 0.5, 0.0);
		glRotatef(alpha, 1.9, 0.6, 0);
		glutWireOctahedron();
		glPopMatrix();
		alpha = alpha + 0.5;

	}
	else if (value == 9) {
		static float alpha = 20;

		glPushMatrix();
		glColor3d(0.0, 0.0, 1.0);
		glRotatef(alpha, 1.9, 0.6, 0);
		glutWireTetrahedron();
		glPopMatrix();
		alpha = alpha + 0.5;
	}
	else if (value == 10) {
		static float alpha = 20;

		glPushMatrix();
		glColor3d(1.0, 1.0, 1.0);
		glTranslated(-0.5, -0.5, 0.0);
		glRotatef(alpha, 1.9, 0.6, 0);
		glutWireIcosahedron();

		alpha = alpha + 0.5;
		glPopMatrix();


	}
	glFlush();
}
void Timer(int extra) {
	glutPostRedisplay();
	glutTimerFunc(40, Timer, 0);
}
int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("3D");
	createMenu();     glClearColor(0.0, 0.0, 0.0, 0.0);
	glutDisplayFunc(display);   glutTimerFunc(0, Timer, 0);
	glutMainLoop();
	return EXIT_SUCCESS;
}

