Build instructions

The code has been written and built on Eclipse IDE for C++ developers. References to GL libraries are shipped with the project metadata. 

On Ubuntu hosts, you need to:
- install freeglut3-dev: apt-get install freeglut3-dev
- use Eclipse C++, import the projects from the path where you cloned the repository. Each project will appear as a sub-project.

The project metadata saved along in this repository already contains the necessary project settings. If you are going to use a different tool to develop/build the project, you may have to add the refernces to GL headers and libraries.
