#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include<math.h>
#include<stdio.h>

void Display_my_pot()
{
	static float grade = -10;

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.1, 0.9, 1.0);
	glLoadIdentity();
	glPushMatrix();
	glRotatef(grade, 1, 0, 0);
	glutWireTorus(1, 1, 20, 10);
	glTranslated(0.5, 0, 0);
	glutWireTeapot(0.3);
	glPopMatrix();
	glFlush();
	grade = grade + 0.1;
	glutPostRedisplay();
}

int main(int argc, char** argv)
{

	glutInit(&argc, argv);

	//glutInitWindowSize(640, 480);
	glutInitWindowPosition(10, 10);
	glutCreateWindow("Teapot 4.4");
	glutDisplayFunc(Display_my_pot);

	glutMainLoop();
	return 0;
}
