#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include<math.h>
#include<stdio.h>



void Display_my_pot()
{
	static float alpha = 20;


	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.1, 0.9, 1.0);
	glLoadIdentity();
	glPushMatrix();
	glRotatef(alpha, 1.9, 0.6, 0);
	glutWireTeapot(0.3);
	glPopMatrix();
	glFlush();

	alpha = alpha + 0.1;

	glutPostRedisplay();
}

int main(int argc, char** argv)
{

	glutInit(&argc, argv);

	//glutInitWindowSize(640, 480);
	glutInitWindowPosition(10, 10);
	glutCreateWindow("Teapot");
	glutDisplayFunc(Display_my_pot);

	glutMainLoop();
	return 0;
}
