/* Bilet 1 */

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void display();
void reshape();

GLint wsize = 405;     // multiplu întreg de 9, din motive estetice, declarat global
GLint psize = 405 / 9; // un punct va fi mereu dimensionat la psize = wsize / 9

int main(int argc, char**argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(wsize, wsize);
	glutInitWindowPosition(600, 100);
	glutCreateWindow("Model 1");
	// Init fragment
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glPointSize(psize); // Setează mărime punct la dimensiunea calculată (1/9 din wsize lungimea/latimea ferestrei fereastră)
	glShadeModel(GL_FLAT);
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMainLoop();
	return 0;
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
	// Desenează 9 puncte, plecând de la 6/9 până la 9/9 pe orizontal, respectiv 1/9 -> 3/9 vertical
	GLint x, y;
	for (GLint i=0; i < 3; i++) { // Desenează o matrice. Bucla parcurgere X
		x = (i + 6) * psize + psize / 2;
		for (GLint j=0; j < 3; j++) { // Bucla parcurgere Y
			y = j * psize + psize / 2;
			glColor3ub(rand() % 255, rand() % 255, rand() % 255);
			glVertex2i(x, y);
		}
	}
	glEnd();
	glFlush();
}

void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, (GLdouble)w, 0.0, (GLdouble)h);
}


