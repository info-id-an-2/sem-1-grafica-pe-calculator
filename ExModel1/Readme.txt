Model examen grafica - bilet 1 

1. Caracterizati grafica digitală (2 pct).
	Grafica pe calculator reprezinta acele metode si tehnici de conversie a datelor 
	catre si de la un dispozitiv grafic prin intermediul calculatorului. 
	Aplicarea graficii pe calculator este folosita din următoarele domenii principale:
	 - Vizualizarea informaţiei
	 - Proiectare
	 - Modelare (simulare)
	 - Interfaţă grafică pentru utilizatori GUI
	În prezent cunoaşterea elementelor de bază ale graficii pe calculator este necesară
	- inginerului,
	- omului de ştiinţă,
	- artiştilor plastici,
	- designerilor,
	- fotografilor,
	- pictorilor de animaţie etc.

	În baza tehnologiilor graficii computerizate s-au dezvoltat:
	- Interfaţa de utilizator – GUI (---inteligenta)
	- Proiectarea digitală în arhitectură şi grafica industrială
	- Efecte vizuale specializate, cinematografia digitală
	- Grafica pe computer pentru filme, animaţie, televiziune
	- Reţeaua Internet --- SM
	- Conferinţele video
	- Televiziunea digitală
	- Proiecte multimedia, proiecte interactive
	- Fotografia digitală şi posibilităţile avansate de prelucrare a fotografiei
	- Grafica şi pictura digitală (cu 2 laturi esenţiale – imitarea materialelor 
	  tradiţionale şi noile instrumente de lucru digitale)
	- Vizualizarea datelor ştiinţifice şi de afaceri
	- Jocuri pe calculator, sisteme de realitate virtuală (de ex. simulatoare pentru aviaţie)
	- Sisteme de proiectare automatizată
	- Tomografie computerizată
	- Poligrafia
	- Grafica laser

	Grafica digitală mai este numită uneori grafică de computer, grafică de calculator,
	grafică pe calculator, grafică computerizată.
	
	Grafica digitală este o activitate în care computerul este utilizat pentru 
	sintetizarea şi elaborarea imaginilor, dar şi pentru prelucrarea informaţiei 
	vizuale obţinute din realitatea ce ne înconjoară.
	
	Grafica digitală se divizează în mai multe categorii:
	- Grafica bidimensională (se constituie din grafică raster şi grafică vector)
	- Grafica Fractal - Grafica tridimensională (este des utilizată în animaţie, 
	  spoturi publicitare, jocuri la computer etc.), 
	  CGI (CGI – în engleză imagini, personaje generate de computer)
	- Grafica animată
	- Grafica video
	- Grafica web
	- Grafica Multimedia
	La baza graficii digitale (şi în special grafica bidimensionala) stau doua 
	  tipuri de calculaţii, Raster (sau rastru) şi Vector (vectorială).


2. Caracterizati grafica vectorială (2 pct).
	Grafica vectorială este un procedeu prin care imaginile sunt construite cu 
	ajutorul descrierilor matematice prin care se determină poziţia, lungimea şi 
	direcţia liniilor folosite în desen. Grafica vectoriala e bazată ca principiu 
	pe desen cu ajutorul liniilor calculate pe o suprafaţa. Liniile pot fi drepte 
	sau curbe.
	
	În cazul imaginilor vectoriale fişierul stochează liniile, formele şi culorile 
	care alcătuiesc imaginea, ca formule matematice. Imaginile Vector pot fi mărite 
	şi micşorate fără a pierde calitatea.

	O imagine poate fi modificată prin manipularea obiectelor din care este alcătuită, 
	acestea fiind salvate apoi ca variaţii ale formulelor matematice specifice. 
	Este des utilizata în imagini cu funcţii decorative, caractere-text, logotipuri, 
	infograme, decor, cat şi în proiecte sofisticate 3D, montare video, animaţie, etc.

3. Explicati modelele de culori RGB si CMYK (2 pct).
	Modelul de culori RGB reprezintă amestecul aditiv RGB 
	(din engleză de la Red-Green-Blue, roşu-verde-albastru) – amestec de culori 
	lumină – se utilizează pentru ecran, monitor, televiziune, scanare, aparate 
	foto, design web, animaţie şi altele.
	
	Modelul de culori CMYK reprezintă amestecul substractiv (din engleză - Cyan, 
	Magenta, Yellow, Key) - – amestec de culori pigment – se utilizează pentru 
	proiecte poligrafice destinate tiparului color.

4. Sa se explice fiecare functie OpenGl utilizata in programul urmator (1.5 pct).

#include <GL/glut.h>
void init()
{
	glClearColor (0.0, 0.0, 0.0, 0.0);	// Stabilelește culoarea de ștergere la 0, 0, 0 (model RGB, negru)
	glShadeModel (GL_FLAT);				// Stabilește modelul de umbră la GL_FLAT 
}
void display()
{
	glClear (GL_COLOR_BUFFER_BIT);	// Curăță bufferul de culoare
	glColor3f (1.0, 0.0, 0.0);		// Setează culoarea de desenare în valori float RGB - Rezultă roșu 
	glBegin(GL_POLYGON);			// Începe desenarea unui poligon
	glVertex2f(200.0, 200.0);		// Adaugă vertex cu coordonate 2d Float
	glVertex2f(400.0, 200.0);		// Adaugă vertex cu coordonate 2d Float
	glVertex2f(400.0, 400.0);		// Adaugă vertex cu coordonate 2d Float
	glEnd();						// Termină desenarea curentă (poligonul are 3 puncte)
	glFlush ();						// Forțează execuția comenzilor GL anterioare - 
	                                // se fac vizibile elementele desenate anterior
}

void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
		// Definește viewport cu coordonate stânga jos 0,0 și de lățime w și 
		// înălțime h, conform valorilor primite ca parametri
	glMatrixMode (GL_PROJECTION);
		// Setează stiva de matrici țintă pentru operațiile următoare 
		// Stiva de matrice proiecție
	glLoadIdentity ();
		// Înlocuiește matricea actuală cu matricea unitate
	gluOrtho2D (0.0, (GLdouble) w, 0.0, (GLdouble) h);
		// setează regiune de vizualizare bidimensională ortografică
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);		// Initializeaza libraria glut
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);	// seteaza modul display initial
	glutInitWindowSize (600, 600);	// setează dimensiunea de afisare a ferestrelor ulterioare
	glutInitWindowPosition (100,100); // Seteaza pozitia la care se vor deschide ferestrele ulterioare
	glutCreateWindow (argv[0]);     // Crează fereastra
	init ();						// apelează funcția locală init (glClearColor, glShadeModel)
	glutDisplayFunc(display);		// Setează funcția handler callback pentru afișare
	glutReshapeFunc(reshape);		// Setează funcția handler callback pentru redimensionarea ferestrei
	glutMainLoop();					// Execută bucla principală GL
	return 0;						// ieșire din program
}

5. Sa se scrie setul de comenzi (utilizand OpenGl) care, in urma executiei, vor conduce 
   la fereastra din figura alaturata (1.5 pct). (Similar cu grila Sudoku, grila 3x3 din dreapta jos, 
   culori portocaliu, cyan, maro, roz, galben, verde deschis, gri, rosu, galben deschis)
   Vezi fișier .c