#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3d(0, 1, 1);
	glBegin (GL_POLYGON);
	  glVertex2f(250, 300);
	  glVertex2f(350, 300);
	  glVertex2f(400, 400);
	  glVertex2f(300, 500);
	  glVertex2f(200, 400);
	glEnd();

	glBegin (GL_POLYGON);
	  glVertex2f( 50, 300);
	  glVertex2f(150, 300);
	  glVertex2f(200, 400);
	  glVertex2f(100, 500);
	  glVertex2f(  0, 400);
	glEnd();

	glFlush();

	glBegin(GL_LINES);
	glColor3d(1,1,1);
	  glVertex2f(0, 300);
	  glVertex2f(450, 300);
	  glVertex2f(200, 300);
	  glVertex2f(200, 500);
	  glEnd();

	glBegin(GL_LINES);
	glColor3d(0,1,1);
	  glVertex2f(200, 300);
	  glVertex2f(200, 100);
	  glEnd();


	glColor3d(0,1,0);
	glBegin(GL_POLYGON);
	  glVertex2f(250, 300);
	  glVertex2f(350, 300);
	  glVertex2f(400, 200);
	  glVertex2f(300, 100);
	  glVertex2f(200, 200);
	glEnd();

	glBegin(GL_POLYGON);
	  glVertex2f( 50, 300);
	  glVertex2f(150, 300);
	  glVertex2f(200, 200);
	  glVertex2f(100, 100);
	  glVertex2f(  0, 200);
	glEnd();


	glFlush();
}

void reshape(int w, int h) {
	glViewport (0,0, (GLsizei) w, (GLsizei) h);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D (0.0, (GLdouble) w, 0.0, (GLdouble) h);
}

int main (int argc, char** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize (600, 600);
	glutInitWindowPosition (10, 10);
	glutCreateWindow ("2019 EC");
	glutDisplayFunc (display);
	glutReshapeFunc (reshape);
	glutMainLoop();
	return 0;
}
