#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


void init() {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glPointSize(50.0);
	glShadeModel (GL_FLAT);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT);
	GLfloat Pi = 2 * 3.14;
	GLfloat rad = 0.9;
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1, 1, 0);
	int sectiuni_i = 10;
	glVertex2f (0.0, 0.0);
	for (int i = 0; i <= sectiuni_i; ++i) {
		glVertex2f ( rad * cos(i * Pi / sectiuni_i), rad * sin(i * Pi / sectiuni_i) );
	}
	glColor3f(1, 0, 0);
	rad = 0.5;
	glVertex2f (0.0, 0.0);
	for (int i = 0; i <= sectiuni_i; ++i) {
		glVertex2f ( rad * cos(i * Pi / sectiuni_i), rad * sin(i * Pi / sectiuni_i) );
	}
	glEnd();
	glFlush();
}


int main (int argc, char** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize (400, 400);
	glutInitWindowPosition (400, 100);
	glutCreateWindow ("2019 EC 2");
	init();
	glutDisplayFunc (display);
	glutMainLoop();
	return 0;
}
